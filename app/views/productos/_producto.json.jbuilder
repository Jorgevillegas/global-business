json.extract! producto, :id, :nombre, :estatus, :categoria_id_id, :precio_dolar, :imagen, :created_at, :updated_at
json.url producto_url(producto, format: :json)
