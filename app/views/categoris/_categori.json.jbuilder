json.extract! categori, :id, :nombre, :estatus, :created_at, :updated_at
json.url categori_url(categori, format: :json)
