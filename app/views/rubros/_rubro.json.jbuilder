json.extract! rubro, :id, :nombre, :estatus, :created_at, :updated_at
json.url rubro_url(rubro, format: :json)
