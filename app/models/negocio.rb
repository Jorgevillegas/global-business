class Negocio < ApplicationRecord
	belongs_to :user
	has_many :productos
	before_save :first_estatus

	#has_attached_file :imagen, styles: { medium: "1280x720" , thumb: "800x600" }
	#has_attached_file :imagen

	#validates_attachment_content_type :imagen, :content_type => /\Aimage/
	#validates_attachment_content_type :imagen, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]


    has_attached_file :imagen, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment_content_type :imagen, content_type: /\Aimage\/.*\z/
  
  def first_estatus
     self.estatus = true if estatus.blank?
     
  end

end

