class RubrosController < ApplicationController
  before_action :set_rubro, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @rubros = Rubro.all
    respond_with(@rubros)
  end

  def show
    respond_with(@rubro)
  end

  def new
    @rubro = Rubro.new
    respond_with(@rubro)
  end

  def edit
  end

  def create
    @rubro = Rubro.new(rubro_params)
    @rubro.save
    respond_with(@rubro)
  end

  def update
    @rubro.update(rubro_params)
    respond_with(@rubro)
  end

  def destroy
    @rubro.destroy
    respond_with(@rubro)
  end

  private
    def set_rubro
      @rubro = Rubro.find(params[:id])
    end

    def rubro_params
      params.require(:rubro).permit(:nombre, :estatus)
    end
end
