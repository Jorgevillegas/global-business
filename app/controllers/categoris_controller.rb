class CategorisController < ApplicationController
  before_action :set_categori, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @categoris = Categori.all
    respond_with(@categoris)
  end

  def show
    respond_with(@categori)
  end

  def new
    @categori = Categori.new
    respond_with(@categori)
  end

  def edit
  end

  def create
    @categori = Categori.new(categori_params)
    @categori.save
    respond_with(@categori)
  end

  def update
    @categori.update(categori_params)
    respond_with(@categori)
  end

  def destroy
    @categori.destroy
    respond_with(@categori)
  end

  private
    def set_categori
      @categori = Categori.find(params[:id])
    end

    def categori_params
      params.require(:categori).permit(:nombre, :estatus)
    end
end
