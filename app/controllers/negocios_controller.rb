class NegociosController < ApplicationController
  before_action :set_negocio, only: [:show, :edit, :update,  :destroy]
  #before_action :validate_user, except: [:show , :index]
  before_action :authenticate_user!, except: [:show , :index]
  before_action :solo_creador, only: [:edit, :update,  :destroy]
  respond_to :html

  def index
    @negocios = Negocio.all
    respond_with(@negocios)
  end

  def show
    @productos = Producto.all

    @producto = Producto.new
  end

  def new
    @negocio = Negocio.new
    respond_with(@negocio)
  end

  def edit

  end

  def create
    @negocio = current_user.negocios.new(negocio_params)
    @negocio.save
    respond_with(@negocio)
  end

  def update
    @negocio.update(negocio_params)
    respond_with(@negocio)
  end

  def destroy
    @negocio.destroy
    respond_with(@negocio)
  end

  def solo_creador
    unless @negocio.user_id == current_user.id

      redirect_to negocios_path, notice: "Solo el creador del negocio puede realizar esta acción"
      
    end
  end


  private

    def set_negocio
      @negocio = Negocio.find(params[:id])
    end

    def negocio_params
      params.require(:negocio).permit(:nombre, :estatus, :correo, :user_id , :tipo_documento_legal, :documento_legal, :direccion , :imagen)
    end
end
