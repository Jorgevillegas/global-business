class ProductosController < ApplicationController
  before_action :set_producto, only: [:show, :edit, :update, :destroy]
  before_action :set_negocio

  respond_to :html



  def show
    respond_with(@negocio,@producto)
  end

  def new
    @producto = Producto.new
    respond_with(@negocio,@producto)
  end

  def edit
  end

  def create
    @producto = Producto.new(producto_params)
    @producto.negocio = @negocio
    @producto.save
       respond_with(@negocio,@producto)

  end

  def update
    @producto.update(producto_params)
    respond_with(@producto)
  end

  def destroy
    @producto.destroy
    respond_with(@negocio,@producto)
  end

  private


    def set_negocio
      @negocio = Negocio.find(params[:negocio_id])      
    end

    def set_producto
      @producto = Producto.find(params[:id])
    end

    def producto_params
      params.require(:producto).permit(:nombre, :estatus, :categori_id, :negocio_id , :precio_dolar)
    end
end


