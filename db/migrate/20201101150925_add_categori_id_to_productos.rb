class AddCategoriIdToProductos < ActiveRecord::Migration[6.0]
  def change
  	add_reference :productos, :categori, null: false, foreign_key: true
  end
end
