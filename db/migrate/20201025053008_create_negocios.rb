class CreateNegocios < ActiveRecord::Migration[6.0]
  def change
    create_table :negocios do |t|
      t.string :nombre
      t.boolean :estatus
      t.string :correo
      t.string :tipo_documento_legal
      t.integer :documento_legal
      t.string :direccion


      t.timestamps
    end
  end
end
