class CreateCategoris < ActiveRecord::Migration[6.0]
  def change
    create_table :categoris do |t|
      t.string :nombre
      t.boolean :estatus

      t.timestamps
    end
  end
end
